using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace WpfAppVar13.Models
{
    public partial class ModelBD : DbContext
    {
        public ModelBD()
            : base("name=ModelBD")
        {
        }

        public virtual DbSet<Bezrabotny> Bezrabotny { get; set; }
        public virtual DbSet<Kontackt_info> Kontackt_info { get; set; }
        public virtual DbSet<Passport> Passport { get; set; }
        public virtual DbSet<Uchet> Uchet { get; set; }
        public virtual DbSet<users> users { get; set; }
        public virtual DbSet<Vakans> Vakans { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Bezrabotny>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<Bezrabotny>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<Bezrabotny>()
                .Property(e => e.Patronymic)
                .IsUnicode(false);

            modelBuilder.Entity<Bezrabotny>()
                .Property(e => e.StudyPlace)
                .IsUnicode(false);

            modelBuilder.Entity<Bezrabotny>()
                .Property(e => e.StudyAddres)
                .IsUnicode(false);

            modelBuilder.Entity<Bezrabotny>()
                .Property(e => e.StudyType)
                .IsUnicode(false);

            modelBuilder.Entity<Bezrabotny>()
                .HasOptional(e => e.Kontackt_info)
                .WithRequired(e => e.Bezrabotny);

            modelBuilder.Entity<Bezrabotny>()
                .HasOptional(e => e.Passport)
                .WithRequired(e => e.Bezrabotny);

            modelBuilder.Entity<Bezrabotny>()
                .HasOptional(e => e.Uchet)
                .WithRequired(e => e.Bezrabotny);

            modelBuilder.Entity<Bezrabotny>()
                .HasOptional(e => e.Vakans)
                .WithRequired(e => e.Bezrabotny);

            modelBuilder.Entity<Kontackt_info>()
                .Property(e => e.Addres)
                .IsUnicode(false);

            modelBuilder.Entity<Kontackt_info>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<Passport>()
                .Property(e => e.Passport1)
                .IsUnicode(false);

            modelBuilder.Entity<Passport>()
                .Property(e => e.Region)
                .IsUnicode(false);

            modelBuilder.Entity<Uchet>()
                .Property(e => e.Registrar)
                .IsUnicode(false);

            modelBuilder.Entity<Uchet>()
                .Property(e => e.Payment)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Uchet>()
                .Property(e => e.Archivist)
                .IsUnicode(false);

            modelBuilder.Entity<users>()
                .Property(e => e.login)
                .IsUnicode(false);

            modelBuilder.Entity<users>()
                .Property(e => e.password)
                .IsUnicode(false);

            modelBuilder.Entity<users>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<Vakans>()
                .Property(e => e.JobType)
                .IsUnicode(false);

            modelBuilder.Entity<Vakans>()
                .Property(e => e.JobName)
                .IsUnicode(false);

            modelBuilder.Entity<Vakans>()
                .Property(e => e.JobGiver)
                .IsUnicode(false);

            modelBuilder.Entity<Vakans>()
                .Property(e => e.Place)
                .IsUnicode(false);

            modelBuilder.Entity<Vakans>()
                .Property(e => e.Mobile)
                .IsUnicode(false);

            modelBuilder.Entity<Vakans>()
                .Property(e => e.District)
                .IsUnicode(false);

            modelBuilder.Entity<Vakans>()
                .Property(e => e.Money)
                .HasPrecision(19, 4);
        }
    }
}
