namespace WpfAppVar13.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Uchet")]
    public partial class Uchet
    {
        [StringLength(15)]
        public string Registrar { get; set; }

        [Column(TypeName = "date")]
        public DateTime? RegDate { get; set; }

        [Column(TypeName = "money")]
        public decimal? Payment { get; set; }

        public bool Experience { get; set; }

        public string Comment { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ArchivesDate { get; set; }

        [StringLength(15)]
        public string Archivist { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UchetID { get; set; }

        public virtual Bezrabotny Bezrabotny { get; set; }
    }
}
