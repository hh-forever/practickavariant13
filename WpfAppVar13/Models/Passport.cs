namespace WpfAppVar13.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Passport")]
    public partial class Passport
    {
        [Column("Passport")]
        [StringLength(20)]
        public string Passport1 { get; set; }

        [Column(TypeName = "date")]
        public DateTime? PassportDate { get; set; }

        [StringLength(40)]
        public string Region { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PassportID { get; set; }

        public virtual Bezrabotny Bezrabotny { get; set; }
    }
}
