namespace WpfAppVar13.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Bezrabotny")]
    public partial class Bezrabotny
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int JoblessID { get; set; }

        [StringLength(20)]
        public string LastName { get; set; }

        [StringLength(20)]
        public string FirstName { get; set; }

        [StringLength(20)]
        public string Patronymic { get; set; }

        public int? Age { get; set; }

        [StringLength(60)]
        public string StudyPlace { get; set; }

        [StringLength(60)]
        public string StudyAddres { get; set; }

        [StringLength(15)]
        public string StudyType { get; set; }

        public virtual Kontackt_info Kontackt_info { get; set; }

        public virtual Passport Passport { get; set; }

        public virtual Uchet Uchet { get; set; }

        public virtual Vakans Vakans { get; set; }
    }
}
