namespace WpfAppVar13.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Kontackt_info
    {
        [StringLength(60)]
        public string Addres { get; set; }

        [StringLength(15)]
        public string Phone { get; set; }

        [Column(TypeName = "image")]
        public byte[] Pictures { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int KontacktID { get; set; }

        public virtual Bezrabotny Bezrabotny { get; set; }
    }
}
