namespace WpfAppVar13.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Vakans
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int JobID { get; set; }

        [StringLength(20)]
        public string JobType { get; set; }

        [StringLength(20)]
        public string JobName { get; set; }

        [StringLength(20)]
        public string JobGiver { get; set; }

        [StringLength(60)]
        public string Place { get; set; }

        [StringLength(15)]
        public string Mobile { get; set; }

        [StringLength(15)]
        public string District { get; set; }

        [Column(TypeName = "money")]
        public decimal? Money { get; set; }

        public string More { get; set; }

        public virtual Bezrabotny Bezrabotny { get; set; }
    }
}
