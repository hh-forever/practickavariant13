﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfAppVar13
{
    /// <summary>
    /// Логика взаимодействия для WindowHome.xaml
    /// </summary>
    public partial class WindowHome : Window
    {
        public static Models.ModelBD bd = new Models.ModelBD();
        public WindowHome()
        {
            InitializeComponent();
            WindowLogin.bd.Bezrabotny.Load();
            familia.ItemsSource = WindowLogin.bd.Bezrabotny.Local;
        }
        Models.Bezrabotny bezrab = new Models.Bezrabotny();

        private void backToMainWindow_Click(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
            this.Close();

        }

        private void saveProfile_Click(object sender, RoutedEventArgs e)
        {
            WindowHome.bd.SaveChanges();
            MainWindow main = new MainWindow();
            main.Show();
            this.Close();
        }

        private void familia_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var Imya = (Models.Bezrabotny)familia.SelectedItem;
            imya.Text = Imya.FirstName;
            var Ucheb = (Models.Bezrabotny)familia.SelectedItem;
            ucheb.Text = Ucheb.StudyPlace;
            var Visschee = (Models.Bezrabotny)familia.SelectedItem;
            visschee.Text = Visschee.StudyType;
        }
    }
}
