﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfAppVar13
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            WindowLogin.bd.Vakans.Load();
            tableVacans.ItemsSource = WindowLogin.bd.Vakans.Local;
            var h = WindowLogin.bd.Vakans.Local.Select(x => x.JobType).Distinct();
            categoryCB.ItemsSource = h.ToList();
            

        }

        private void findButton_Click(object sender, RoutedEventArgs e)
        {
            

        }

        private void homeButton_Click(object sender, RoutedEventArgs e)
        {
            WindowHome main = new WindowHome();
            main.Show();
            this.Close();

        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            categoryCB.Text = null;
            tableVacans.ItemsSource = WindowLogin.bd.Vakans.Local;
        }

        private void viborVakans_Click(object sender, RoutedEventArgs e)
        {

        }

        private void categoryCB_DropDownClosed(object sender, EventArgs e)
        {
            tableVacans.ItemsSource = WindowLogin.bd.Vakans.Local.Where(f => f.JobType == categoryCB.Text).ToList();
        }

        private void ColumnBT_Click(object sender, RoutedEventArgs e)
        {

        }

        private void tableVacans_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
